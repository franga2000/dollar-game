var CACHE_NAME = 'f2k-DollarGame';
var urlsToCache = [
	'/',
	'/style.css',
	'/main.js',
	'/game.js'
];


self.addEventListener('install', function(event) {
	// Perform install steps
	event.waitUntil(
		// Enumerate levels
		fetch('/levels/index')
		.then(resp => resp.text())
		.then(text => {
			let index = text.split("\n").map(name => '/levels/' + name);
			for (level of index) {
				urlsToCache.push(level);
			}
		})
		.then(() => {
			// Start caching
			caches.open(CACHE_NAME)
			.then(function(cache) {
				console.log('Opened cache');
				return cache.addAll(urlsToCache);
			})
		})
	);

});


self.addEventListener('fetch', function(event) {
	event.respondWith(
		caches.match(event.request)
		.then(function(response) {
			// Cache hit - return response
			if (response) {
				return response;
			}
				return fetch(event.request);
			}
		)
	);
});

