
class Vertex {
	constructor(id, initialBalance) {
		this.id = id;
		this.connections = [];
		this.initialBalance = initialBalance
		
		game.vizNodes.add({
			id: this.id,
			label: this.balance
		})
	}
	
	set initialBalance(_) {}
	
	get balance() {
		return this._balance;
	}
	
	set balance(balance) {
		this._balance = balance;
		
		let color = balance < 0 ? "#ff5252" : "#00e676";
		
		game.vizNodes.update({ 
			id:this.id, 
			label: balance + "€", 
			color: color
		});
	}
	
	give() {
		for (let vert of this.connections) {
			vert.balance = vert.balance + 1;
		}
		
		this.balance = this.balance - this.connections.length;
		
		game.giveLog.push(this.id);
		
		document.getElementById("movelog").innerHTML += `<li><strong>${this.id}</strong> gave to <strong>${this.connections.map(v => v.id).join(", ") }</strong></li>`;
	}
	
}

class Game {
	
	constructor(level) {
		this.giveLog = [];

		this.verts = [];

		this.vizNodes = new vis.DataSet([]);
		this.vizEdges = new vis.DataSet([]);
		
		this.loadLevel(level).then(function() {
			game.initViz();
		});
	}
	
	loadLevel(levelPath) {
		console.debug("Loading level " + levelPath);
		let req = fetch(levelPath).then(res => res.text())
		req.then(function(text) {
			eval(text);
			
			console.debug("Level loaded");
		});
		
		return req;
	}
	
	initViz() {
		this.options = {
			interaction: {
				hover: true
			},
			layout: {
				randomSeed: 2
			},
			nodes: {
				heightConstraint: 25,
				widthConstraint: 25
			}
		};

		this.container = document.getElementById("viz");
		var data = {
			nodes: this.vizNodes,
			edges: this.vizEdges
		};
		
		this.network = new vis.Network(this.container, data, this.options);

		
		this.network.on("click", function (params) {
			if (params.nodes.length < 1)
				return;
			
			let id = params.nodes[0];
			game.verts[id].give();
			
			game.network.unselectAll();
			
			if (game.hasWon()) {
				alert("You win in " + game.giveLog.length + " moves!");
			}
		});
	}
	
	createVert(balance) {
		let vert = new Vertex(this.verts.length);
		vert.balance = balance;
		this.verts.push(vert);	
	}
	
	connectVerts(v1, v2) {
		v1.connections.push(v2);
		v2.connections.push(v1);
		
		// Viz
		this.vizEdges.add({
			from: v1.id,
			to: v2.id,
			color: { color: "#CCC" }
		});
	}
	
	hasWon() {
		for (let vert of this.verts) {
			if (vert.balance < 0) {
				return false;
			}
		}
		return true;
	}
	
	
}


let game = new Game("levels/numberphile.js");



